#include <iostream>
#include <map>
#include <string>
#include <functional>

#include "XMLSerializable.h"
#include "Armor.h"
#include "Weapon.h"
#include "Enemy.h"
#include "Consumable.h"
#include "Player.h"

using namespace std;

XMLSerializable* constructItem(){

        return new Item;
}

//map<string,function<XMLSerializable*()>> mapConstructor;
//std::map< std::string, XMLSerializable* (*fp)() > mapConstructor;

/*int main(){
	mapConstructor["Entity"] = [](){ return new Entity;};
	mapConstructor["Item"] = [](){ return new Item;};
	mapConstructor["Unit"] = [](){ return new Unit;};
	mapConstructor["Enemy"] = [](){ return new Enemy;};
	mapConstructor["Player"] = [](){ return new Player;};
	mapConstructor["Consumable"] = [](){ return new Consumable;};
	mapConstructor["Weapon"] = [](){ return new Weapon;};
	mapConstructor["Armor"] = [](){ return new Armor;};

	cout << "Give me an string" <<endl;
	string sLookup;
	cin >> sLookup;

	//XMLSerializable* (*pFunc)() = mapConstructor[sLookup];
	function<XMLSerializable*()> pFunc = mapConstructor[sLookup];

	if(pFunc == NULL){
		//do stuff if we didn't find anything else
	}else{
		//do stuff if we did find something in the map
	}

	return 0;
}*/
