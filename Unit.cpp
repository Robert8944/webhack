#include "Unit.h"

using namespace std;

Unit::Unit(){
	level = 0;
	health = 0;
}

void Unit::setLevel(int sLevel){
	level = sLevel;
}
int Unit::getLevel(){
	return level;
}

void Unit::setHealth(int sHealth){
	health = sHealth;
}
int Unit::getHealth(){
	return health;
}

void Unit::attack(Unit & target){
	//perform damage
}

void Unit::dumpObject(){
	cout << "This is a Unit with the following data:" << endl;
	dumpObjectData();
}

void Unit::dumpObjectData(){
	Entity::dumpObjectData();
	cout << "	Level: " << level << endl;
	cout << "	Health: " << health << endl;
}

void Unit::writeFragment(ostream & output){
	output << "	<Unit>" << endl;
	writeDataAsFragment(output);
	output << "	</Unit>" << endl;
}

/*<Weapon>
	<name>dagger</name>
	<weight>5</weight>
	<value>10</value>
	<displayChar>(</displayChar>
	<damage>3</damage>
	<type>piercing</type>
</Weapon>*/

void Unit::writeDataAsFragment(ostream & output){
	Entity::writeDataAsFragment(output);
	output << "		<level>" << level << "</level>" << endl;
	output << "		<health>" << health << "</health>" << endl;
}

void Unit::setElementData(string sElementName, string sValue){
	if(sElementName == "Level" || sElementName == "level"){
		level = atoi(sValue.c_str());
	}else if(sElementName == "Health" || sElementName == "health"){
		health = atoi(sValue.c_str());
	}else{
		Entity::setElementData(sElementName,sValue);
	}
}