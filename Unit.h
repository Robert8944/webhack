#ifndef Unit_included
#define Unit_included

#include "Entity.h"

class Unit : public Entity{
public:
	Unit();

	virtual void setLevel(int sLevel);
	virtual int getLevel();

	virtual void setHealth(int sHealth);
	virtual int getHealth();

	virtual void attack(Unit & target);

	virtual void dumpObject();
	virtual void dumpObjectData();
	virtual void writeFragment(std::ostream & output);
	virtual void writeDataAsFragment(std::ostream & output);
	virtual void setElementData(std::string sElementName, std::string sValue);
private:
	int level;
	int health;
};

#endif 