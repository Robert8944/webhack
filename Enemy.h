#ifndef Enemy_included
#define Enemy_included

#include "Unit.h"

class Enemy : public Unit{
public:
	Enemy();

	virtual void setHostile(bool sHostile);
	virtual bool getHostile();

	virtual void takeTurn();
	
	virtual void dumpObject();
	virtual void dumpObjectData();
	virtual void writeFragment(std::ostream & output);
	virtual void writeDataAsFragment(std::ostream & output);
	virtual void setElementData(std::string sElementName, std::string sValue);
private:
	bool hostile;
};

#endif 