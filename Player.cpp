#include "Player.h"

using namespace std;

Player::Player(){
	experience = 0;
}

void Player::setexperience(int sexperience){
	experience = sexperience;
}
int Player::getexperience(){
	return experience;
}

void Player::dumpObject(){
	cout << "This is a Player with the following data:" << endl;
	dumpObjectData();
}

void Player::dumpObjectData(){
	Unit::dumpObjectData();
	cout << "	Experience: " << experience << endl;
}

void Player::writeFragment(ostream & output){
	output << "	<Player>" << endl;
	writeDataAsFragment(output);
	output << "	</Player>" << endl;
}

/*<Weapon>
	<name>dagger</name>
	<weight>5</weight>
	<value>10</value>
	<displayChar>(</displayChar>
	<damage>3</damage>
	<type>piercing</type>
</Weapon>*/

void Player::writeDataAsFragment(ostream & output){
	Unit::writeDataAsFragment(output);
	output << "		<experience>" << experience << "</experience>" << endl;
}

void Player::setElementData(string sElementName, string sValue){
	if(sElementName == "Experience" || sElementName == "experience"){
		experience = atoi(sValue.c_str());
	}else{
		Unit::setElementData(sElementName,sValue);
	}
}