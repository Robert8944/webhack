#ifndef Consumable_included
#define Consumable_included

#include "Item.h"

class Consumable : public Item{
public:
	Consumable();

	virtual void setHealth(int sHealth);
	virtual int getHealth();

	virtual void setNutrition(int sNutrition);
	virtual int getNutrituion();

	virtual void use(Unit & user);

	virtual void dumpObject();
	virtual void dumpObjectData();
	virtual void writeFragment(std::ostream & output);
	virtual void writeDataAsFragment(std::ostream & output);
	virtual void setElementData(std::string sElementName, std::string sValue);
private:
	int health;
	int nutrition;
};

#endif 