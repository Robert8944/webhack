#include "Consumable.h"

using namespace std;

Consumable::Consumable(){
	health = 0;
	nutrition = 0;
}

void Consumable::setHealth(int sHealth){
	health = sHealth;
}
int Consumable::getHealth(){
	return health;
}

void Consumable::setNutrition(int sNutrition){
	nutrition = sNutrition;
}
int Consumable::getNutrituion(){
	return nutrition;
}

void Consumable::use(Unit & user){
	setQuantity(getQuantity()-1);
}

void Consumable::dumpObject(){
	cout << "This is a Consumable with the following data:" << endl;
	dumpObjectData();
}

void Consumable::dumpObjectData(){
	Item::dumpObjectData();
	cout << "	Health: " << health << endl;
	cout << "	Nutrition: " << nutrition << endl;
}

void Consumable::writeFragment(ostream & output){
	output << "	<Consumable>" << endl;
	writeDataAsFragment(output);
	output << "	</Consumable>" << endl;
}

/*<Weapon>
	<name>dagger</name>
	<weight>5</weight>
	<value>10</value>
	<displayChar>(</displayChar>
	<damage>3</damage>
	<type>piercing</type>
</Weapon>*/

void Consumable::writeDataAsFragment(ostream & output){
	Item::writeDataAsFragment(output);
	output << "		<health>" << health << "</health>" << endl;
	output << "		<nutrition>" << nutrition << "</nutrition>" << endl;
}

void Consumable::setElementData(string sElementName, string sValue){
	if(sElementName == "Nutrition" || sElementName == "nutrition"){
		nutrition = atoi(sValue.c_str());
	}else if(sElementName == "Health" || sElementName == "health"){
		health = atoi(sValue.c_str());
	}else{
		Item::setElementData(sElementName,sValue);
	}
}