#include "Entity.h"

using namespace std;

Entity::Entity(){
	Entity::name = "";
	Entity::displayChar = 'a';
}

void Entity::setName(std::string sName){
	name = sName;
}
std::string Entity::getName(){
	return name;
}

void Entity::setDisplayChar(char sdChar){
	displayChar = sdChar;
}
char Entity::getDisplayChar(){
	return displayChar;
}

void Entity::dumpObject(){
	cout << "This is an Entity with the following data:" << endl;
	dumpObjectData();
}

void Entity::dumpObjectData(){
	XMLSerializable::dumpObjectData();
	cout << "	Name: " << name << endl;
	cout << "	Display Character: " << displayChar << endl;
}

void Entity::writeFragment(ostream & output){
	output << "	<Entity>" << endl;
	writeDataAsFragment(output);
	output << "	</Entity>" << endl;
}

/*<Weapon>
	<name>dagger</name>
	<weight>5</weight>
	<value>10</value>
	<displayChar>(</displayChar>
	<damage>3</damage>
	<type>piercing</type>
</Weapon>*/

void Entity::writeDataAsFragment(ostream & output){
	XMLSerializable::writeDataAsFragment(output);
	output << "		<name>" << name << "</name>" << endl;
	output << "		<displayChar>" << displayChar << "</displayChar>" << endl;
}

void Entity::setElementData(string sElementName, string sValue){
	if(sElementName == "Name" || sElementName == "name"){
		name = sValue;
	}else if(sElementName == "DisplayChar" || sElementName == "displayChar"){
		displayChar = sValue.c_str()[0];
	}else{
		XMLSerializable::setElementData(sElementName,sValue);
	}
}