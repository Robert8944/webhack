#include "Enemy.h"

using namespace std;

Enemy::Enemy(){
	hostile = false;
}

void Enemy::setHostile(bool sHostile){
	hostile = sHostile;
}
bool Enemy::getHostile(){
	return hostile;
}

void Enemy::takeTurn(){
	//perform enemy logic
}

void Enemy::dumpObject(){
	cout << "This is an Enemy with the following data:" << endl;
	dumpObjectData();
}

void Enemy::dumpObjectData(){
	Unit::dumpObjectData();
	cout << "	Hostility: " << hostile << endl;
}

void Enemy::writeFragment(ostream & output){
	output << "	<Enemy>" << endl;
	writeDataAsFragment(output);
	output << "	</Enemy>" << endl;
}

/*<Weapon>
	<name>dagger</name>
	<weight>5</weight>
	<value>10</value>
	<displayChar>(</displayChar>
	<damage>3</damage>
	<type>piercing</type>
</Weapon>*/

void Enemy::writeDataAsFragment(ostream & output){
	Unit::writeDataAsFragment(output);
	output << "		<hostile>" << hostile << "</hostile>" << endl;
}

void Enemy::setElementData(string sElementName, string sValue){
	if(sElementName == "Hostile" || sElementName == "hostile"){
		if(sValue == "1" || sValue == "true"){
			hostile = true;
		}else if(sValue == "0" || sValue == "false"){
			hostile = false;
		}else{
			cout << "Value not understood" << endl;
		}
	}else{
		Unit::setElementData(sElementName,sValue);
	}
}