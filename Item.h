#ifndef Item_included
#define Item_included

#include "Entity.h"
#include "Unit.h"

class Item : public Entity{
public:
	Item();

	virtual void setWeight(int sWeight);
	virtual int getWeight();

	virtual void setValue(int sValue);
	virtual int getValue();

	virtual void setQuantity(int sQuantity);
	virtual int getQuantity();

	virtual void use(Unit & user);

	virtual void dumpObject();
	virtual void dumpObjectData();
	virtual void writeFragment(std::ostream & output);
	virtual void writeDataAsFragment(std::ostream & output);
	virtual void setElementData(std::string sElementName, std::string sValue);
private:
	int weight;
	int value;
	int quantity;
};

#endif 