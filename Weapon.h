#ifndef Weapon_included
#define Weapon_included

#include "Item.h"

class Weapon : public Item{
public:
	Weapon();

	virtual void setDamage(int sDamage);
	virtual int getDamage();

	virtual void setType(std::string sType);
	virtual std::string getType();

	virtual void dumpObject();
	virtual void dumpObjectData();
	virtual void writeFragment(std::ostream & output);
	virtual void writeDataAsFragment(std::ostream & output);
	virtual void setElementData(std::string sElementName, std::string sValue);
private:
	int damage;
	std::string type;
};

#endif 