#ifndef Armor_included
#define Armor_included

#include "Item.h"

class Armor : public Item{
public:
	Armor();

	virtual void setProtection(int sProtection);
	virtual int getProtection();

	virtual void setType(std::string sType);
	virtual std::string getType();

	virtual void dumpObject();
	virtual void dumpObjectData();
	virtual void writeFragment(std::ostream & output);
	virtual void writeDataAsFragment(std::ostream & output);
	virtual void setElementData(std::string sElementName, std::string sValue);
private:
	int protection;
	std::string type;
};

#endif 