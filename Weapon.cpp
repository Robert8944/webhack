#include "Weapon.h"

using namespace std;

Weapon::Weapon(){
	damage = 0;
	type = "";
}

void Weapon::setDamage(int sDamage){
	damage = sDamage;
}
int Weapon::getDamage(){
	return damage;
}

void Weapon::setType(std::string sType){
	type = sType;
}
std::string Weapon::getType(){
	return type;
}

void Weapon::dumpObject(){
	cout << "This is a Item with the following data:" << endl;
	dumpObjectData();
}

void Weapon::dumpObjectData(){
	Item::dumpObjectData();
	cout << "	Damage: " << damage << endl;
	cout << "	Type: " << type << endl;
}

void Weapon::writeFragment(ostream & output){
	output << "	<Weapon>" << endl;
	writeDataAsFragment(output);
	output << "	</Weapon>" << endl;
}

/*<Weapon>
	<name>dagger</name>
	<weight>5</weight>
	<value>10</value>
	<displayChar>(</displayChar>
	<damage>3</damage>
	<type>piercing</type>
</Weapon>*/

void Weapon::writeDataAsFragment(ostream & output){
	Item::writeDataAsFragment(output);
	output << "		<damage>" << damage << "</damage>" << endl;
	output << "		<type>" << type << "</type>" << endl;
}

void Weapon::setElementData(string sElementName, string sValue){
	if(sElementName == "Damage" || sElementName == "damage"){
		damage = atoi(sValue.c_str());
	}else if(sElementName == "Type" || sElementName == "type"){
		type = atoi(sValue.c_str());
	}else{
		Item::setElementData(sElementName,sValue);
	}
}