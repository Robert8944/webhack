#ifndef Player_included
#define Player_included

#include "Unit.h"

class Player : public Unit{
public:
	Player();

	virtual void setexperience(int sexperience);
	virtual int getexperience();

	virtual void dumpObject();
	virtual void dumpObjectData();
	virtual void writeFragment(std::ostream & output);
	virtual void writeDataAsFragment(std::ostream & output);
	virtual void setElementData(std::string sElementName, std::string sValue);
private:
	int experience;
};

#endif 