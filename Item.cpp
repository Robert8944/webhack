#include "Item.h"

using namespace std;

Item::Item(){
	weight = 0;
	value = 0;
	quantity = 0;
}

void Item::setWeight(int sWeight){
	weight = sWeight;
}
int Item::getWeight(){
	return weight;
}

void Item::setValue(int sValue){
	value = sValue;
}
int Item::getValue(){
	return value;
}

void Item::setQuantity(int sQuantity){
	quantity = sQuantity;
}
int Item::getQuantity(){
	return quantity;
}

void Item::use(Unit & user){
	quantity--;
	//do something to player based on item
}

void Item::dumpObject(){
	cout << "This is an Item with the following data:" << endl;
	dumpObjectData();
}

void Item::dumpObjectData(){
	Entity::dumpObjectData();
	cout << "	Weight: " << weight << endl;
	cout << "	Quantity: " << quantity << endl;
	cout << "	Value: " << value << endl;
}

void Item::writeFragment(ostream & output){
	output << "	<Item>" << endl;
	writeDataAsFragment(output);
	output << "	</Item>" << endl;
}

/*<Weapon>
	<name>dagger</name>
	<weight>5</weight>
	<value>10</value>
	<displayChar>(</displayChar>
	<damage>3</damage>
	<type>piercing</type>
</Weapon>*/

void Item::writeDataAsFragment(ostream & output){
	Entity::writeDataAsFragment(output);
	output << "		<weight>" << weight << "</weight>" << endl;
	output << "		<quantity>" << quantity << "</quantity>" << endl;
	output << "		<value>" << value << "</value>" << endl;
}

void Item::setElementData(string sElementName, string sValue){
	if(sElementName == "Weight" || sElementName == "weight"){
		weight = atoi(sValue.c_str());
	}else if(sElementName == "Quantity" || sElementName == "quantity"){
		quantity = atoi(sValue.c_str());
	}else if(sElementName == "Value" || sElementName == "value"){
		value = atoi(sValue.c_str());
	}else{
		Entity::setElementData(sElementName,sValue);
	}
}