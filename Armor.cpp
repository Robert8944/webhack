#include "Armor.h"

using namespace std;

Armor::Armor(){
	protection = 0;
	type = "";
}

void Armor::setProtection(int sProtection){
	protection = sProtection;
}
int Armor::getProtection(){
	return protection;
}

void Armor::setType(std::string sType){
	type = sType;
}
std::string Armor::getType(){
	return type;
}
void Armor::dumpObject(){
	cout << "This is armor with the following data:" << endl;
	dumpObjectData();
}

void Armor::dumpObjectData(){
	Item::dumpObjectData();
	cout << "	Type: " << type << endl;
	cout << "	Protection: " << protection << endl;
}

void Armor::writeFragment(ostream & output){
	output << "	<Armor>" << endl;
	writeDataAsFragment(output);
	output << "	</Armor>" << endl;
}

/*<Weapon>
	<name>dagger</name>
	<weight>5</weight>
	<value>10</value>
	<displayChar>(</displayChar>
	<damage>3</damage>
	<type>piercing</type>
</Weapon>*/

void Armor::writeDataAsFragment(ostream & output){
	Item::writeDataAsFragment(output);
	output << "		<type>" << type << "</type>" << endl;
	output << "		<protection>" << protection << "</protection>" << endl;
}

void Armor::setElementData(string sElementName, string sValue){
	if(sElementName == "Protection" || sElementName == "protection"){
		protection = atoi(sValue.c_str());
	}else if(sElementName == "Type" || sElementName == "type"){
		type = sValue;
	}else{
		Item::setElementData(sElementName,sValue);
	}
}