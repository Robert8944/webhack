#ifndef Entity_included
#define Entity_included

#include <string>

#include "XMLSerializable.h"

class Entity : public XMLSerializable{
public:
	Entity();

	virtual void setName(std::string sName);
	virtual std::string getName();

	virtual void setDisplayChar(char sdChar);
	virtual char getDisplayChar();
	
	virtual void dumpObject();
	virtual void dumpObjectData();
	virtual void writeFragment(std::ostream & output);
	virtual void writeDataAsFragment(std::ostream & output);
	virtual void setElementData(std::string sElementName, std::string sValue);
private:
	std::string name;
	char displayChar;
};

#endif
