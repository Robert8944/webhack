#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <functional>
#include <map>

#include "XMLSerializable.h"
#include "Item.h"
#include "Armor.h"
#include "Weapon.h"
#include "Enemy.h"
#include "Consumable.h"
#include "Player.h"

using namespace std;

vector<XMLSerializable*> things;

map<string,function<XMLSerializable*()>> mapConstructor;

/*void parseElement(istream & input, string sPrefix){
	
	string sElementName;
	getline(input, sElementName, '>');
	cout << sPrefix << sElementName << " (start)" << endl;
	bool bDone = false;
	string sContent;
	while( !bDone ){
		char c = input.get();
		if( c == '<' ){
			if( input.peek() == '/' ){
				string sEndTag;
				getline(input, sEndTag, '>');
				if( sEndTag == "/" + sElementName ){
					cout << sPrefix << sElementName << ": " << sContent << " (end)" << endl;
					bDone = true;
				}
				else{
					cout << "Inavlid XML!" << endl;
					return;
				}
			}else{
				parseElement(input, sPrefix + "-> ");
			}
		}else if( c != '\n' ){
			sContent.push_back(c);
		}
	}
}*/

void parseElement(istream & input, int pre){
	if(pre == 0){
		string sElementName;
		getline(input, sElementName, '>');
		//cout << sPrefix << sElementName << " (start)" << endl;
		bool bDone = false;
		string sContent;
		while( !bDone ){
			char c = input.get();
			if( c == '<' ){
				if( input.peek() == '/' ){
					string sEndTag;
					getline(input, sEndTag, '>');
					if( sEndTag == "/" + sElementName ){
						//cout << sPrefix << sElementName << ": " << sContent << " (end)" << endl;
						bDone = true;
					}
					else{
						cout << "Inavlid XML!" << endl;
						return;
					}
				}else{
					parseElement(input, pre + 1);
				}
			}else if( c != '\n' ){
				sContent.push_back(c);
			}
		}
	}else if(pre == 1){
		string sElementName;
		getline(input, sElementName, '>');
		//cout << sPrefix << sElementName << " (start)" << endl;
		things.push_back(mapConstructor[sElementName]());
		bool bDone = false;
		string sContent;
		while( !bDone ){
			char c = input.get();
			if( c == '<' ){
				if( input.peek() == '/' ){
					string sEndTag;
					getline(input, sEndTag, '>');
					if( sEndTag == "/" + sElementName ){
						//cout << sPrefix << sElementName << ": " << sContent << " (end)" << endl;
						bDone = true;
					}
					else{
						cout << "Inavlid XML!" << endl;
						return;
					}
				}else{
					parseElement(input, pre + 1);
				}
			}else if( c != '\n' ){
				sContent.push_back(c);
			}
		}
	}else if(pre == 2){
		string sElementName;
		getline(input, sElementName, '>');
		//cout << sPrefix << sElementName << " (start)" << endl;
		bool bDone = false;
		string sContent;
		while( !bDone ){
			char c = input.get();
			if( c == '<' ){
				if( input.peek() == '/' ){
					string sEndTag;
					getline(input, sEndTag, '>');
					if( sEndTag == "/" + sElementName ){
						//cout << sPrefix << sElementName << ": " << sContent << " (end)" << endl;
						things.back()->setElementData(sElementName, sContent);
						bDone = true;
					}
					else{
						cout << "Inavlid XML!" << endl;
						return;
					}
				}else{
					parseElement(input, pre + 1);
				}
			}else if( c != '\n' ){
				sContent.push_back(c);
			}
		}
	}
}

void parseXML(istream & input){
	while(input.get() != '<'){

	}
	if( input.get() != '?' ){
		cout << "Invalid header" << endl;
		return;
	}
	while( input.get() != '?'){

	}
	if( input.get() != '>'){
                cout << "Invalid header" << endl;
                return;
	}
	while(input.get() != '<'){

	}

	parseElement(input, 0);
}



int main(int argc, char * argv[]){

	mapConstructor["Entity"] = [](){ return new Entity;};
	mapConstructor["Item"] = [](){ return new Item;};
	mapConstructor["Unit"] = [](){ return new Unit;};
	mapConstructor["Enemy"] = [](){ return new Enemy;};
	mapConstructor["Player"] = [](){ return new Player;};
	mapConstructor["Consumable"] = [](){ return new Consumable;};
	mapConstructor["Weapon"] = [](){ return new Weapon;};
	mapConstructor["Armor"] = [](){ return new Armor;};

	string sFilename;
	cout << "What file should we read? ";
	cin >> sFilename;

	ifstream input;
	input.open(sFilename.c_str());

	parseXML(input);

	for(size_t i = 0; i < things.size(); i++){
		things.at(i)->dumpObject();
	}

	cout << "What file should we print to? ";
	cin >> sFilename;

	ofstream output;
	output.open(sFilename.c_str());

	output << "<?xml version=\"1.0\" encoding=\"utf-8\"?>" << endl;
	output << "<World>" << endl;
	for(size_t i = 0; i < things.size(); i++){
		things.at(i)->writeFragment(output);
	}
	output << "</World>" << endl;

	cin.ignore(200,'\n');
	return 0;
}
/*int main (int argc, char * argv[]){
	//Item item;//program 1 stuff
	//item.setName("Wombat");
	//cout << "This item is a " << item.getName() << "." << endl;
	

	cout << "Give me the name of an XML file to read: ";
	string fileName;
	cin >> fileName;


	cout << "Give me the name of an XML file to write to: ";
	cin >> fileName;
	ofstream outputF;
	outputF.open(fileName);
	outputF << "<?xml version=\"1.0\" encoding=\"utf-8\"?>" << endl;
	//for(int i = 0; i < 

	outputF.close();
	return 0;
}*/
